#! /usr/bin/env nix-shell
#! nix-shell -i python3
import telebot
import markovify
import ujson
import requests
import re
from lxml import html
from dataclasses import dataclass, asdict, field
from collections import defaultdict
import signal
import sys
import random
import threading
import traceback
import os
import dotenv
import subprocess
from typing import Dict
from peewee import *
from telebot import apihelper

db = SqliteDatabase('db.sqlite')

class ChatModel(Model):
    chat_id = BigIntegerField(primary_key=True)
    model = BlobField(default=None)
    last_msg = TextField(default="")
    ignore_bots = BooleanField(default=True)
    divisor = SmallIntegerField(default=20)
    state_size = SmallIntegerField(default=2)

    class Meta:
        database = db

@dataclass
class Chat():
    chat_id : int
    model : markovify.Text = None
    last_msg : str = None
    ignore_bots : bool = True
    divisor : int = 20
    state_size : int = 2
    code_messages: Dict[int, int] = field(default_factory=dict)

    def generate_message(self):
        topics = self.model.word_split(self.last_msg) or [""]
        for i in range(self.state_size, 0, -1):
            msg = self.model.make_sentence_with_start("".join(topics[:i]), strict=False, tries=20)
            if msg: return msg

        msg = self.model.make_sentence_with_start(topics[0], strict = False, tries=20)
        if msg: return msg
        return self.model.make_short_sentence(200, tries=20)

    def update(self, message):
        message = message.strip()
        message = re.sub(r"(\w)\n", "\g<1>. ", message)
        message = re.sub(r"(\w)$", "\g<1>.", message)

        self.last_msg = message

        for line in filter(len, message.split("\n")):
            try:
                update = markovify.Text(line, state_size=self.state_size)
            except:
                return

            if self.model == None:
                self.model = update
            else:
                self.model = markovify.combine([self.model, update])

    def load_html_history(self, raw):
        tree = html.fromstring(raw)

        n = 0

        # text_content doesn't replace br's with newlines
        for br in tree.xpath("*//br"):
            br.tail = "\n" + br.tail if br.tail else "\n"

        messages = tree.xpath("//div[@class='message default clearfix']/div[@class='body']/div[@class='text']")
        for message in messages:
            n += 1
            self.update(message.text_content())
        return n

    @db.atomic()
    def save(self):
        dump = asdict(self)
        dump['model'] = dump['model'].to_json()

        instance, created = ChatModel.get_or_create(chat_id=self.chat_id, defaults=dump)
        if created is False:
            for key in dump:
                if key != "code_messages": # TODO: shitty move
                    setattr(instance, key, dump[key])

        instance.save()


    @classmethod
    @db.atomic()
    def load(cls, chat_id):
        try:
            instance = ChatModel.get_by_id(chat_id)
            return Chat(
                chat_id = instance.chat_id,
                model = markovify.Text.from_json(instance.model),
                last_msg = instance.last_msg,
                ignore_bots = instance.ignore_bots,
                divisor = instance.divisor,
                state_size = instance.state_size,
            )
        except DoesNotExist:
            return Chat(chat_id)

dotenv.load_dotenv()
API_TOKEN = os.environ['API_TOKEN']

if "SOCKS" in os.environ:
    apihelper.proxy = {"https":os.environ["SOCKS"]}

bot = telebot.TeleBot(API_TOKEN)

chats = dict()

def get_chat(chat_id):
    if chat_id not in chats:
        chats[chat_id] = Chat.load(chat_id)
    return chats[chat_id]

@bot.message_handler(commands=['divisor'])
def set_divisor(message):
    chat = get_chat(message.chat.id)

    try:
        divisor = int(message.text.split(" ")[1])
    except:
        divisor = chat.divisor

    chat.divisor = divisor
    chat.save()

    bot.reply_to(message, f"Шанс нюхнуть попца выставлен как 1 к {divisor}.")

@bot.message_handler(commands=['butt'])
def send_butt(message):
    res = requests.get('http://api.obutts.ru/butts/1/1/random')
    photo = requests.get(f"http://media.obutts.ru/{res.json()[0]['preview']}")
    bot.send_photo(message.chat.id, photo.content)

@bot.message_handler(regexp="(^\/boobs($|\s)|(С|с)иськи)")
def send_boobs(message):
    res = requests.get('http://api.oboobs.ru/boobs/1/1/random')
    photo = requests.get(f"http://media.oboobs.ru/{res.json()[0]['preview']}")
    if re.search("(С|с)иськи", message.text):
        bot.send_photo(message.chat.id, photo.content, reply_to_message_id=message.message_id, caption="Сиськи!")
    else:
        bot.send_photo(message.chat.id, photo.content)

@bot.message_handler(content_types=['document'])
def load_history(message):
    if message.document.mime_type != "text/html":
        return
    chat = get_chat(message.chat.id)
    file_info = bot.get_file(message.document.file_id)
    file = requests.get('https://api.telegram.org/file/bot{0}/{1}'.format(API_TOKEN, file_info.file_path))
    if file.status_code == 200:
        file.encoding = "UTF-8"
        n = chat.load_html_history(file.text)
        if n > 0:
            bot.reply_to(message, f"Я нашёл тут {n} сообщений и дохуища обучился.")

def is_code(lang):
    def test_lang(message):
        return message.text.startswith(lang) and any(map(lambda e: e.type == "code" or e.type == "pre", message.entities))
    return test_lang

# @bot.edited_message_handler(func=is_code("py"))
# @bot.message_handler(func=is_code("py"))
# def handle_py(message):
#     chat = get_chat(message.chat.id)
#     code_blocks = list(filter(lambda e: e.type == "code" or e.type == "pre", message.entities))
#     for block in code_blocks:
#         try:
#             result = subprocess.run([
#                                         "firejail",
#                                         "--private",
#                                         "--private-tmp",
#                                         f"--rlimit-as={5 * 10**8}",
#                                         "--private-dev",
#                                         "--quiet",
#                                         "--nice=19",
#                                         "python3", "-c",
#                                         message.text[block.offset:block.offset+block.length]
#                                     ],
#                                     text=True,
#                                     timeout=3,
#                                     capture_output=True)
#             if result.stdout:
#                 reply_text = f"```\n{result.stdout}```"
#             else:
#                 reply_text = "No output."
#             if str(result.stderr):
#                 reply_text = f"{reply_text}\n\nstderr is non-empty:\n```\n{result.stderr}```"
# 
#         except subprocess.TimeoutExpired:
#             reply_text = "Time limit."
# 
#         if message.message_id in chat.code_messages:
#             try:
#                 bot.edit_message_text(reply_text, chat_id=chat.chat_id, message_id=chat.code_messages[message.message_id], parse_mode="Markdown")
#             except:
#                 pass
#         else:
#             sent = bot.reply_to(message, reply_text, parse_mode="Markdown")
#             chat.code_messages[message.message_id] = sent.message_id

@bot.message_handler()
def process_message(message):
    chat = get_chat(message.chat.id)
    chat.update(message.text)

    will_reply = False
    if message.reply_to_message:
        will_reply = message.reply_to_message.from_user.id == bot.get_me().id

    will_reply = will_reply or random.random() < (1 / chat.divisor)
    if will_reply:
        reply = chat.generate_message()
        if reply:
            bot.reply_to(message, reply)

def sigint_handler(sig, frame):
    print("Cautght SIGINT, saving chats")
    for chat in chats.values():
        print(f"Saving chat {chat.chat_id}")
        chat.save()
    db.close()
    sys.exit(0)

signal.signal(signal.SIGINT, sigint_handler)

db.connect()
db.create_tables([ChatModel])


bot.polling()
