let
  pkgs = import <nixpkgs> {};

  _telebot = with pkgs.python37Packages; buildPythonPackage rec {
    pname = "pyTelegramBotAPI";
    version = "3.6.6";

    src = pkgs.python37.pkgs.fetchPypi {
      inherit pname version;
      sha256 = "00vycd7jvfnzmvmmhkjx9vf40vkcrwv7adas5i81r2jhjy7sks54";
    };

    doCheck = false;

    propagatedBuildInputs = with pkgs.python37Packages; [ requests six wheel ];
  };

  _markovify = with pkgs.python37Packages; buildPythonPackage rec {
    pname = "markovify";
    version = "0.7.2";

    src = pkgs.python37.pkgs.fetchPypi {
      inherit pname version;
      sha256 = "08px29axlh0nll084c7dmg62ar7zjsdxj3nmwav2yjchknpkbznh";
    };

    doCheck = false;

    propagatedBuildInputs = with pkgs.python37Packages; [ unidecode ];
  };

in pkgs.stdenv.mkDerivation rec {
  name = "versor";

  buildInputs = with pkgs; [
    stdenv
    (python37.withPackages (ps: [ps.python-dotenv ps.lxml ps.setuptools ps.peewee ps.ujson ps.requests _markovify _telebot]))
    libxml2.dev
    libxslt.dev
  ];
}
